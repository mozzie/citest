<div class="container">
	<div class="success alert alert-success" style="display: none;"></div>
	<div class="error alert alert-danger" style="display: none;"></div>

	<form class="form-address" id="form-address">
		<h2 class="form-address-heading">Please fill the form</h2>
		<div class="form-group">
			<label for="city">City</label>
			<select name="city_id" class="form-control">
				<option>Please select city</option>
				<?php if(!empty($cities)): ?>
					<?php foreach($cities as $city): ?>
						<option value="<?php echo $city->id ?>"><?php echo $city->name ?></option>
					<?php endforeach; ?>
				<?php endif; ?>
			</select>
		</div>
		<div class="form-group">
			<label for="street">Street</label>
			<input type="text" class="form-control" id="street" name="street" placeholder="Ex: Main Street">
		</div>
		<div class="row">
			<div class="form-group col-md-6 col-sm-12">
				<label for="house_number">House</label>
				<input type="text" class="form-control" id="house_number" name="house_number" placeholder="12/2">
			</div>
			<div class="form-group col-md-6 col-sm-12">
				<label for="apt_number">Apartment #</label>
				<input type="text" class="form-control" id="apt_number" name="apt_number" placeholder="4">
			</div>
		</div>
		<button type="submit" class="btn btn-primary">Save</button>
	</form>
</div>