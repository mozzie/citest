<?php

class Address_model extends CI_Model
{
	/** @var string  */
	protected $table = 'address';

	public $formRules = [
		[
			'field' => 'city_id',
			'label' => 'City',
			'rules' => 'trim|required',
		],
		[
			'field' => 'street',
			'label' => 'Street',
			'rules' => 'trim|required',
		],
		[
			'field' => 'house_number',
			'label' => 'House',
			'rules' => 'trim|required',
		],
		[
			'field' => 'apt_number',
			'label' => 'Apartments',
			'rules' => 'trim|required',
		]
	];

	/**
	 * @param $data
	 * @return mixed
	 */
	public function save($data)
	{
		return $this->db->insert($this->table, $data);
	}
}