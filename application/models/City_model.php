<?php

class City_model extends CI_Model
{
	/** @var string  */
	protected $table = 'city';

	/**
	 * @return mixed
	 */
	public function selectAll()
	{
		$query = $this->db->get($this->table);
		return $query->result();
	}
}