<?php

class Welcome extends CI_Controller
{
	public function form()
	{
		$this->load->model('City_model', 'city');

		$this->load->view('layout', [
			'cities' => $this->city->selectAll()
		]);
	}
}