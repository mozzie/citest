<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends CI_Controller
{
	public function save()
	{
		$this->load->library('form_validation');
		$this->load->model('Address_model', 'address');

		$this->form_validation->set_rules($this->address->formRules);
		if(!$this->form_validation->run()) {
			$validationErrors = [];
			$this->response([
				'success'	=> false,
				'errors'	=> implode("; ", $this->form_validation->error_array())
			]);
		}

		$data = $this->input->post();

		$result = $this->address->save($data);
		if(!$result) {
			$this->response(['success' => false, 'errors' => ['save to db failed']]);
		}

		$this->response(['success' => true, 'msg' => 'saved successfully']);
	}

	/**
	 * @param $resp
	 */
	protected function response($resp)
	{
		$this->output->set_content_type('application/json')->set_output(json_encode($resp))->_display();
		die;
	}
}
