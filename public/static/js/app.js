(function($){
	$(function() {
		$('#form-address').on('submit', function(e) {
			e.preventDefault();
			e.stopPropagation();

			var $form = $(this);
			var formData = $form.serialize();

			$.post('/address/save', formData, function(res) {
				var $successBlock = $('.success'),
					$errorBlock = $('.error');

				if(!res.success) {
					var errors = res.errors ? res.errors : 'something went wrong';
					$errorBlock.html(errors).fadeIn();
					$successBlock.fadeOut();

					return;
				}

				$errorBlock.fadeOut();
				$successBlock.text('Saved').fadeIn();
			});
		})
	});
})(jQuery);